// In Pursuit of Greed Mod - Doom Modification for GZDoom
// Copyright (C) 2020-2025  William Weber Berrutti (aka Cyantusk)

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

Actor Witch : IPOGEnemyBase 22204
{
    Health 60
    Radius 16
    Height 56
    Scale 0.42
    Speed 5
    Mass 150
    -CANUSEWALLS
    -CANPUSHWALLS
    PainChance 230
    ReactionTime 16
    SeeSound "sorceress/see"
    PainSound "sorceress/pain"
    DeathSound "sorceress/death"
    Obituary "%o was terrified by a Witch."
    States
    {
    Spawn:
        SOR3 AAAABBBBAAAACCCCDDDDCCCCAAAABBBBAAAACCCCDDDDCCCC 1    // 48 tics (so she can turn on a random direction properly)
        {
            A_LookEx;
            A_Wander;
        }
    SpawnContinue:
        SOR3 AAAABBBBAAAACCCCDDDDCCCC 1
        {
            A_LookEx;
            A_Wander(CHF_NORANDOMTURN);
        }
        Loop
    See:
        TNT1 A 0 A_ChangeFlag(CANUSEWALLS, 1)
        TNT1 A 0 A_ChangeFlag(CANPUSHWALLS, 1)
        SOR3 AAAABBBBAAAACCCCDDDDCCCC 1 A_Chase
        Loop
    Searching:
        TNT1 A 0 A_JumpIfInventory("IPOGAggressivenessCounter", 6 / (CountInv("IPOGFastCounter") + 1), "Searching_RemoveCounter")
        SOR3 AAAABBBBAAAACCCCDDDDCCCC 1 A_Chase("","")
        TNT1 A 0 A_GiveInventory("IPOGAggressivenessCounter", 1)
        Loop
    Searching_RemoveCounter:
        TNT1 A 0 A_TakeInventory("IPOGAggressivenessCounter", 6-GetCVar("skill"))
        Goto See
    Idle:
        SOR3 AAAABBBBAAAACCCCDDDDCCCC 1
        {
            A_LookEx;
            A_Wander;
        }
        Loop
    Melee:
        SORC EEEEEE 1 A_FaceTarget
        TNT1 A 0 A_PlaySound("wizard/melee",CHAN_AUTO)
        SOR3 E 0 A_CustomMeleeAttack(4)
        SOR3 E 1 Bright Light("GenericAttackLight_Cyan")
        {
            A_FaceTarget;
            A_SpawnItemEx("WitchProjectileTrail",16,0,44,0,0,0,0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION);
            A_SpawnItemEx("WitchProjectileTrail",16+FRandom(-1,1),FRandom(-1,1),44+FRandom(-1,1),0,0,0,0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION);
            A_SpawnItemEx("WitchProjectileTrail",16+FRandom(-1,1),FRandom(-1,1),44+FRandom(-1,1),0,0,0,0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION);
            A_SpawnItemEx("WitchProjectileTrail",16+FRandom(-1,1),FRandom(-1,1),44+FRandom(-1,1),0,0,0,0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION);
        }
        SOR3 E 1 Bright Light("GenericAttackLight_Cyan") A_FaceTarget
        TNT1 A 0 A_MonsterRefire(0, "See")
        Goto MissileEnd
    Missile:
        SOR3 EEEEEEEEEEEEEEE 1 Bright
        {
            A_FaceTarget;
            A_SpawnItemEx("WitchProjectileTrail",16,0,44,FRandom(-1.5,1.5),FRandom(-1.5,1.5),FRandom(-1.5,1.5),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION);
        }
        SOR3 E 0 A_PlaySound("sorceress/attack")
        SOR3 E 8 Bright A_SpawnProjectile("WitchProjectile", 44, 0, 0, CMF_AIMOFFSET)
        SOR3 E 4 A_FaceTarget
    MissileEnd:
        TNT1 A 0 A_Jump(230 - (38 * GetCVar("skill")), "Searching")
        Goto See
        
    Pain:
        SOR3 F 5 A_Pain
        Goto MissileEnd
    Death:
        SOR3 G 3 A_ScreamAndUnblock
        SOR3 HIJKLM 3
        SOR3 N -1 A_KillMaster("", KILS_FOILINVUL)
        Stop
    Death.Crush:
    XDeath:
        TNT1 A 0 A_Stop
        TNT1 A 0 A_SpawnItemEx("XDeathGibs_Height56")
        SOR3 G 0 A_ScreamAndUnblock
        TNT1 A 0 A_SpawnItemEx("SorceressHead", 0, 0, 49, FRandom(-12,12), FRandom(-12,12), FRandom(2,18), 0, SXF_NOCHECKPOSITION|SXF_TRANSFERTRANSLATION)
        TNT1 AA 0 A_SpawnItemEx("SorceressHand", 0, 0, 28, FRandom(-12,12), FRandom(-12,12), FRandom(-18,18), 0, SXF_NOCHECKPOSITION|SXF_TRANSFERTRANSLATION)
        TNT1 AA 0 A_SpawnItemEx("SorceressFoot", 0, 0, 14, FRandom(-12,12), FRandom(-12,12), FRandom(-18,18), 0, SXF_NOCHECKPOSITION|SXF_TRANSFERTRANSLATION)
        TNT1 A -1 A_KillMaster("", KILS_FOILINVUL)
        Stop
    Raise:
        SOR3 NMLKJIHG 3
        Goto See
    }
}

Actor WitchProjectile
{
    Speed 16
    Radius 5
    Height 5
    Scale 0.65
    PROJECTILE
    Species "Enemy"
    DamageType "SorceressMagic"
    Damage (10)
    RenderStyle "Add"
    Alpha 0.9
    -THRUSPECIES
    +SEEKERMISSILE
    +FORCEXYBILLBOARD
    +THRUGHOST
    +DONTSEEKINVISIBLE
    +FORCERADIUSDMG
    BounceType "Hexen"
    BounceCount 2
    +USEBOUNCESTATE
    States
    {
    Bounce:
        TNT1 A 0 A_PlaySound("sorceress/projhit")
        TNT1 AAAAAAAA 0 A_SpawnItemEx("WitchProjectileTrail",0,0,0,FRandom(-0.75,0.75),FRandom(-0.75,0.75),FRandom(-0.75,0.75),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION)
        TNT1 A 0 A_JumpIfTargetInLOS("Seek", 360, JLOSF_PROJECTILE)
        Goto Spawn_StraightTrail
    Spawn:
        WITP B 1 Bright Light("WitchProjectile")
        {
            A_SpawnItemEx("WitchProjectileTrail",0,0,0,FRandom(-1.5,1.5),FRandom(-1.5,1.5),FRandom(-1.5,1.5),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION);
        }
        Loop
    Spawn_StraightTrail:
        WITP B 1 Bright Light("WitchProjectile")
        {
            A_SpawnItemEx("WitchProjectileTrail",0,0,0,velx*0.25,vely*0.25,velz*0.25,0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION);
        }
        Loop
    Seek:
        WITP B 1 Bright Light("WitchProjectile")
        {
            A_SeekerMissile(90,90,SMF_LOOK|SMF_PRECISE,256,3);
            A_SeekerMissile(90,90,SMF_LOOK|SMF_PRECISE,256,3);
            A_SpawnItemEx("WitchProjectileTrail",0,0,0,velx*0.25,vely*0.25,velz*0.25,0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION);
        }
        Goto Spawn_StraightTrail
    Death:
    Crash:
    XDeath:
        TNT1 A 0 A_Explode(5,32,0,0,16)
        TNT1 A 0 A_PlaySound("sorceress/projhit")
        TNT1 AAAAAAAA 0 A_SpawnItemEx("WitchProjectileTrail",0,0,0,FRandom(-1.5,1.5),FRandom(-1.5,1.5),FRandom(-1.5,1.5),0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION)
        Stop
    }
}

Actor WitchProjectileTrail : IPOGProjectileEffectBase
{
    +THRUACTORS
    Scale 0.525
    States
    {
    Spawn:
        TNT1 A 2
    SpawnContinue:
        WITP B 1 Bright A_FadeOut(0.125)
        Loop
    }
}