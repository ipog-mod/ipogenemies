[Symbol_Game]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Sprites/Other/IJWLC0
[Symbol_Important]: https://gitlab.com/ipog-mod/ipogclasses/-/raw/master/Graphics/IPOGFORW


# IPOG Enemies Pack

### ![Symbol_Important] IMPORTANT: This requires [IPOG Classes](https://gitlab.com/ipog-mod/ipogclasses) to work!

## ![Symbol_Game]Description

It's an add-on pack that has enemies from "In Pursuit of Greed," including many custom ones made based on it. It replaces vanilla Doom 2 and vanilla Heretic monsters.


## ![Symbol_Game]Compatibility

Classic Doom source ports supporting ACS and DECORATE: GZDoom 4.2.4 or above (except 4.3 ones), LZDoom 3.84 or above (except 3.85). It might work on older versions.


## ![Symbol_Game]Custom CVar(s)

#### ![Symbol_Important] NOTE: All CVars are dynamic, meaning they all take effect after changed or as soon as possible.

**IPOG_Spawn <value\>:** Spawns a set of foes according to <value\>:

        0: Random Variants: It spawns foes from all sets.
        1: Desarian Orbital Penal Colony variants: A riot is in progress, so Prisoners may hurt and alert enemies to attack them!
        2: City Temple of Ristanak variants: No event mechanic is happening, but that doesn't mean it'll be easy!
        3: Mt. Kaal's Jump Base variants: Probes may be patrolling the map, meaning they may alert nearby enemies if they notice you!

**IPOG_WeaponDrops <0|1\>:** Weapons are dropped from low tier enemies, simulating Doom's original pattern. The weapons are:

        Plasma Spreadgun: Trooper, Sorceress, KMan
        Heavy Chaingun: Trooper2, Prophetess, KMan2
        Flame Thrower: Trooper3, Berserker, KMan3


## ![Symbol_Game]Licensing

In Pursuit of Greed Enemies project is licensed under the [GPL-3](http://www.gnu.org/licenses/gpl.html).

The data files (artwork, sound files, etc) are not covered in this license. The ones that came from In Pursuit of Greed have a [custom license](https://archive.org/details/GreedSource):

"These sources are provided for educational and historical purposes. No assets or code may be used in any way commercially. Personal and educational use only."

